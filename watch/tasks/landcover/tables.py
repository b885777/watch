facc_description = {
    'AL020': 'Built-Up Area',
    'AP030': 'Road',
    'BA040': 'Water',
    'BH082': 'Inland Waterbody',
    'BH135': 'Rice Field',
    'BH140': 'River or Stream',
    'BH160': 'Sebkha',
    'BJ100': 'Snow or Ice Field',
    'DA010': 'Ground',
    'DB170': 'Sand Dune',
    'EA010': 'Cropland',
    'EB010': 'Grassland',
    'EB070': 'Brush',
    'EC015': 'Forest',
    'ED010': 'Marsh'
}
