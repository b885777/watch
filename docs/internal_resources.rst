While WATCH is designed and intended to be a fully open source system, some
aspects of the WATCH module are developed in private. Additionally, there are
some data sources which will not be made public.

This page is meant to provide references to these resources that require
authentication to our internal collaberators. If you do not have credentials
you will not be able to access these resources.

+----------------------------------------------------------+----------------------------------------------------------------+
| The Internal SMART WATCH Python Module                   | https://gitlab.kitware.com/smart/watch/                        |
+----------------------------------------------------------+----------------------------------------------------------------+
| The Internal SMART WATCH DVC Repo                        | https://gitlab.kitware.com/smart/smart_watch_dvc/              |
+----------------------------------------------------------+----------------------------------------------------------------+
| The Internal SMART WATCH Infastructure Docs (DEPRECATED) | https://gitlab.kitware.com/smart/watch-infrastructure          |
+----------------------------------------------------------+----------------------------------------------------------------+
| The Internal SMART WATCH Girder Collection (DEPRECATED)  | https://data.kitware.com/#collection/602457272fa25629b95d1718  |
+----------------------------------------------------------+----------------------------------------------------------------+
